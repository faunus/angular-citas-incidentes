'use strict';

/**
 * @ngdoc overview
 * @name directivesApp
 * @description
 * # directivesApp
 *
 * Main module of the application.
 */
var jamon = angular
  .module('directivesApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/app/quotes', {
        templateUrl: 'views/app/create_quote.html',
        controllerAs: 'ctrl',
        controller: [function () {
          var self = this;

          self.genders = [
            {id: 1, name: 'Female'},
            {id: 2, name: 'Male'}
          ];

          self.employment_statuses = [
            {id: 1, name: 'Employed'},
            {id: 2, name: 'Full-time student'}
          ];

          self.marital_statuses = [
            {id: 1, name: 'Single'},
            {id: 2, name: 'Married'},
            {id: 3, name: 'Widowed'}
          ];

          self.driver_occupations = [
            {id: 1, name: 'Plumber'},
            {id: 2, name: 'Carpenter'},
            {id: 3, name: 'Other'}
          ];

          self.incident_kinds = [
            {id: 1, name: 'Violation'},
            {id: 1, name: 'Accident'},
          ];

          self.vehicle_years = [
            {id: 1, name: 2010},
            {id: 2, name: 2011},
            {id: 3, name: 2012},
          ];

          self.vehicles = [
            {id: 1, year: 2011, name: 'HONDA'},
            {id: 2, year: 2012, name: 'ACURA'},
            {id: 3, year: 2012, name: 'ACURA'},
            {id: 4, year: 2012, name: 'AUDI'},
          ];
        }]
      })
      .when('/web/about', {
        templateUrl: 'views/web/about.html',
      })
      .otherwise({
        redirectTo: '/app/quotes'
      });
  });
