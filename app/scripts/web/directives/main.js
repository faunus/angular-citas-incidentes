'use strict';

/**
 * @ngdoc function
 * @name directivesApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the directivesApp
 */
angular.module('directivesApp')
  .directive('web', [function () {
    return {
      restrict: 'E',
      templateUrl: 'views/web/page.html',
    };
  }]);
