'use strict';

angular.module('directivesApp')
    .directive('populateQuoteForm', [function(){
        return {
            scope: {
                quoteGenders: '=',
                quoteEmploymentStatuses: '=',
                quoteMaritalStatuses: '=',
                quoteDriverOcupations: '=',
                quoteIncidentKinds: '=',
                quoteVehicleYears: '=',
                quoteVehicles: '=',
            },
            templateUrl: 'views/app/populate_quote_form.html',
            controller: 'PopulateQuoteForm',
            controllerAs: 'ctrl',
            //bindToController: true
        };
    }])
    .controller('PopulateQuoteForm', [function() {

    }]);
